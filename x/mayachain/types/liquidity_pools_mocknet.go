//go:build mocknet
// +build mocknet

package types

import (
	"gitlab.com/mayachain/mayanode/common"
)

var LiquidityPools = common.Assets{
	common.BTCAsset,
	common.BNBAsset,
	common.ETHAsset,
}
