package keeperv1

import (
	. "gopkg.in/check.v1"

	"gitlab.com/mayachain/mayanode/common"
	cosmos "gitlab.com/mayachain/mayanode/common/cosmos"
)

type KeeperLiquidityProviderSuite struct{}

var _ = Suite(&KeeperLiquidityProviderSuite{})

func (mas *KeeperLiquidityProviderSuite) SetUpSuite(c *C) {
	SetupConfigForTest()
}

func (s *KeeperLiquidityProviderSuite) TestLiquidityProvider(c *C) {
	ctx, k := setupKeeperForTest(c)
	asset := common.BNBAsset

	lp, err := k.GetLiquidityProvider(ctx, asset, GetRandomBaseAddress())
	c.Assert(err, IsNil)
	c.Check(lp.PendingCacao, NotNil)
	c.Check(lp.Units, NotNil)

	lp = LiquidityProvider{
		Asset:        asset,
		Units:        cosmos.NewUint(12),
		CacaoAddress: GetRandomBaseAddress(),
		AssetAddress: GetRandomBTCAddress(),
	}
	k.SetLiquidityProvider(ctx, lp)
	lp, err = k.GetLiquidityProvider(ctx, asset, lp.CacaoAddress)
	c.Assert(err, IsNil)
	c.Check(lp.Asset.Equals(asset), Equals, true)
	c.Check(lp.Units.Equal(cosmos.NewUint(12)), Equals, true)
	iter := k.GetLiquidityProviderIterator(ctx, common.BNBAsset)
	c.Check(iter, NotNil)
	iter.Close()
	k.RemoveLiquidityProvider(ctx, lp)
}

func (s *KeeperLiquidityProviderSuite) TestLiquidityAuctionTier(c *C) {
	ctx, k := setupKeeperForTest(c)
	newLATier := LiquidityAuctionTier{
		Address: GetRandomBaseAddress(),
	}

	// Tier not previously set
	laTierValue, err := k.GetLiquidityAuctionTier(ctx, newLATier.Address)
	c.Assert(err, IsNil)
	c.Assert(laTierValue, Equals, int64(0))

	// It should set the tier if it's equals or lower
	err = k.SetLiquidityAuctionTier(ctx, newLATier.Address, 3)
	c.Assert(err, IsNil)
	laTierValue, err = k.GetLiquidityAuctionTier(ctx, newLATier.Address)
	c.Assert(err, IsNil)
	c.Assert(laTierValue, Equals, int64(3))

	err = k.SetLiquidityAuctionTier(ctx, newLATier.Address, 2)
	c.Assert(err, IsNil)
	laTierValue, err = k.GetLiquidityAuctionTier(ctx, newLATier.Address)
	c.Assert(err, IsNil)
	c.Assert(laTierValue, Equals, int64(2))

	err = k.SetLiquidityAuctionTier(ctx, newLATier.Address, 1)
	c.Assert(err, IsNil)
	laTierValue, err = k.GetLiquidityAuctionTier(ctx, newLATier.Address)
	c.Assert(err, IsNil)
	c.Assert(laTierValue, Equals, int64(1))
}
