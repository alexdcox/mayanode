package mayachain

import (
	"fmt"

	"gitlab.com/mayachain/mayanode/common/cosmos"
)

func (h DonateHandler) validateV80(ctx cosmos.Context, msg MsgDonate) error {
	if err := msg.ValidateBasic(); err != nil {
		return err
	}
	if msg.Asset.IsSyntheticAsset() {
		ctx.Logger().Error("asset cannot be synth", "error", errInvalidMessage)
		return errInvalidMessage
	}

	if isLiquidityAuction(ctx, h.mgr.Keeper()) {
		pool, err := h.mgr.Keeper().GetPool(ctx, msg.Asset)
		if err != nil {
			return ErrInternal(err, fmt.Sprintf("fail to get pool(%s)", msg.Asset))
		}

		if pool.Status != PoolAvailable {
			return errInvalidPoolStatus
		}
	}

	return nil
}

func (h DonateHandler) handleV1(ctx cosmos.Context, msg MsgDonate) error {
	pool, err := h.mgr.Keeper().GetPool(ctx, msg.Asset)
	if err != nil {
		return ErrInternal(err, fmt.Sprintf("fail to get pool for (%s)", msg.Asset))
	}
	if pool.Asset.IsEmpty() {
		return cosmos.ErrUnknownRequest(fmt.Sprintf("pool %s not exist", msg.Asset.String()))
	}

	if isLiquidityAuction(ctx, h.mgr.Keeper()) {
		err = h.addLiquidityFromDonate(ctx, msg, pool)
		if err != nil {
			return ErrInternal(err, "fail to add liquidity with donate memo")
		}
	} else {
		pool.BalanceAsset = pool.BalanceAsset.Add(msg.AssetAmount)
		pool.BalanceCacao = pool.BalanceCacao.Add(msg.CacaoAmount)

		if err := h.mgr.Keeper().SetPool(ctx, pool); err != nil {
			return ErrInternal(err, fmt.Sprintf("fail to set pool(%s)", pool))
		}
	}

	// emit event
	donateEvt := NewEventDonate(pool.Asset, msg.Tx)
	if err := h.mgr.EventMgr().EmitEvent(ctx, donateEvt); err != nil {
		return cosmos.Wrapf(errFailSaveEvent, "fail to save donate events: %w", err)
	}
	return nil
}
